﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ParnicaV2.Models;
using ParnicaV2.Services;

namespace ParnicaV2.Controllers
{
    public class ProcedureTypesController : Controller
    {
        ServiceProcedureType service = new ServiceProcedureType();

        [Authorize(Roles = ("Admin"))]
        public ActionResult Index()
        {
            return View(service.List());
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcedureType tipPostupka = service.GetById(id);
            if (tipPostupka == null)
            {
                return HttpNotFound();
            }
            return View(tipPostupka);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Create([Bind(Include = "Id,Name")] ProcedureType tipPostupka)
        {
            if (ModelState.IsValid)
            {
                service.Add(tipPostupka);
                return RedirectToAction("Index");
            }

            return View(tipPostupka);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcedureType tipPostupka = service.GetById(id);
            if (tipPostupka == null)
            {
                return HttpNotFound();
            }
            return View(tipPostupka);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit([Bind(Include = "Id,Name")] ProcedureType tipPostupka)
        {
            if (ModelState.IsValid)
            {
                service.Edit(tipPostupka);
                return RedirectToAction("Index");
            }
            return View(tipPostupka);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcedureType tipPostupka = service.GetById(id); 
            if (tipPostupka == null)
            {
                return HttpNotFound();
            }
            return View(tipPostupka);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult DeleteConfirmed(int id)
        {
            service.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
