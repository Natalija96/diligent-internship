﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ParnicaV2.Models;
using ParnicaV2.Services;
using ParnicaV2.ViewModels;

namespace ParnicaV2.Controllers
{
    public class LitigationsController : Controller
    {

        ServiceLitigation service;

        public LitigationsController()
        {
            service = new ServiceLitigation(); 
        }

        [Authorize]
        public ActionResult Index(string SearchStrLoc,string SearchStrProcType,
          string SearchStrCourtType, string SearchStrJudge)
        {

            SearchStrLoc = String.IsNullOrEmpty(SearchStrLoc) ? "": SearchStrLoc;
            SearchStrProcType = String.IsNullOrEmpty(SearchStrProcType) ? "": SearchStrProcType;
            SearchStrCourtType = String.IsNullOrEmpty(SearchStrCourtType) ? "": SearchStrCourtType;
            SearchStrJudge = String.IsNullOrEmpty(SearchStrJudge) ? "": SearchStrJudge;  

            ViewData["locFilter"] =  SearchStrLoc;
            ViewData["procTypeFilter"] = SearchStrProcType;
            ViewData["courtTypeFilter"] = SearchStrCourtType;
            ViewData["sudijaFilter"] = SearchStrJudge;

            var  parnice = service.List(s => s.ProcedureType.Name.Contains(SearchStrProcType)
                                         && s.Location.Name.Contains(SearchStrLoc)
                                         && s.CourtType.Contains(SearchStrCourtType)
                                         && s.Judge.Name.Contains(SearchStrJudge));

           
            return View(LitigationViewModelList.ToDTOlist(parnice));

        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Litigation parnica = service.GetById(id);
            if (parnica == null)
            {
                return HttpNotFound();
            }
            return View(LitigationViewModelDetails.ToDTO(parnica));
        }

        [Authorize]
        public ActionResult Create()
        {        
            InitViewBagCreate();
            return View();
        }
     

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(LitigationViewModel litigation, string[] advokati)
        {
            if (ModelState.IsValid && advokati!=null)
            {

                service.Add(LitigationViewModel.FromDTOtoModel(litigation), advokati);
   
                return RedirectToAction("Index");
            }
          
            InitViewBagCreate();
            return View(litigation);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Litigation litigation = service.GetById(id);
            if (litigation == null)
            {
                return HttpNotFound();
            }

            InitViewBagsEdit(litigation);
          
            return View(LitigationViewModel.ToDTO(litigation));
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(LitigationViewModel litigation, string [] advokati, string[] neadvokati)
        {     
            if (ModelState.IsValid)
            {
                service.Edit(LitigationViewModel.FromDTOtoModel(litigation), advokati, neadvokati);
                return RedirectToAction("Index");
            }
            InitViewBagsEdit(service.GetById(litigation.Id));
            return View(litigation);
        }

        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Litigation parnica = service.GetById(id);
            if (parnica == null)
            {
                return HttpNotFound();
            }
            return View(LitigationViewModelDelete.ToDTO(parnica));
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            service.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }

        public void InitViewBagsEdit(Litigation l)
        {
            ViewBag.Kontakti = service.ListContacts();
            ViewBag.TipPostupka = service.ListProcedureTypes();
            ViewBag.Lokacije = service.ListLocations();
            ViewBag.Advokati = UserViewModelForLitigation.toDTOList(l.Lawyers);
            var neAdv = service.ListUsers().Where(x => !l.Lawyers.Contains(x));
            ViewBag.NeAdvokati = UserViewModelForLitigation.toDTOList(neAdv);
        }
        public void InitViewBagCreate()
        {
            ViewBag.Kontakti = service.ListContacts();
            ViewBag.TipPostupka = service.ListProcedureTypes();
            ViewBag.Lokacije = service.ListLocations();
            ViewBag.Korisnici = UserViewModelForLitigation.toDTOList(service.ListUsers());
        }
    }
}
