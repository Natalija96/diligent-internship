using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using DHTMLX.Scheduler;
using DHTMLX.Common;
using DHTMLX.Scheduler.Data;
using DHTMLX.Scheduler.Controls;

using ParnicaV2.Models;
using ParnicaV2.Services;

namespace ParnicaV2.Controllers
{
    public class CalendarController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            var scheduler = new DHXScheduler(this);
            scheduler.Config.xml_date = "%d/%m/%Y %H:%i";
            scheduler.Config.readonly_form = true;
            scheduler.Lightbox.AddDefaults();
  
            scheduler.InitialDate = DateTime.Now.Date; // new DateTime(2020, 09, 03);

            scheduler.LoadData = true;
            scheduler.EnableDataprocessor = true;

            return View(scheduler);
        }

        [Authorize]
        public ContentResult Data()
        {
            ServiceLitigation service = new ServiceLitigation();
            List<CalendarEvent> listEvants = new List<CalendarEvent>();
            foreach (var parnica in service.List())
            {

                listEvants.Add(new CalendarEvent
                {
                    id = parnica.Id,
                    text = parnica.Id.ToString()
                    + " Location: "+ parnica.Location.Name+
                    " Judge: "+parnica.Judge.Name+
                    " Defendant: "+ parnica.Defendant.Name+
                    " Prosecutor: " + parnica.Prosecutor.Name,

                    start_date = new DateTime(parnica.DateAndTime.Year, parnica.DateAndTime.Month, parnica.DateAndTime.Day, parnica.DateAndTime.Hour,
                    parnica.DateAndTime.Minute, parnica.DateAndTime.Second),

                    end_date = new DateTime(parnica.DateAndTime.Year, parnica.DateAndTime.Month, parnica.DateAndTime.Day, parnica.DateAndTime.Hour+2,
                    parnica.DateAndTime.Minute, parnica.DateAndTime.Second)

                });         
            }

            var data = new SchedulerAjaxData( listEvants );
            return (ContentResult)data;
        }

        [Authorize]
        public ContentResult Save(int? id, FormCollection actionValues)
        {
            var action = new DataAction(actionValues);
            
            try
            {
                var changedEvent = (CalendarEvent)DHXEventsHelper.Bind(typeof(CalendarEvent), actionValues);  

             /*   switch (action.Type)
                {
                    case DataActionTypes.Insert:
                        //do insert
                        //action.TargetId = changedEvent.id;//assign postoperational id
                        break;
                    case DataActionTypes.Delete:
                        //do delete
                        break;
                    default:// "update"                          
                        //do update
                        break;
                }*/
            }
            catch
            {
                action.Type = DataActionTypes.Error;
            }
            return (ContentResult)new AjaxSaveResponse(action);
        }
    }
}

