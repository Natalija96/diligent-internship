﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ParnicaV2.Models;
using ParnicaV2.Services;

namespace ParnicaV2.Controllers
{
    public class LocationsController : Controller
    {

        ServiceLocation service = new ServiceLocation();
 
        [Authorize(Roles = ("Admin"))]
        public ActionResult Index()
        {
            return View(service.List());
        }

        // GET: Lokacijas/Details/5
        [Authorize(Roles = ("Admin"))]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location lokacija = service.GetById(id);
            if (lokacija == null)
            {
                return HttpNotFound();
            }
            return View(lokacija);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Create([Bind(Include = "Id,Name")] Location lokacija)
        {
            if (ModelState.IsValid)
            {
                service.Add(lokacija);
                return RedirectToAction("Index");
            }

            return View(lokacija);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location lokacija = service.GetById(id);
            if (lokacija == null)
            {
                return HttpNotFound();
            }
            return View(lokacija);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit([Bind(Include = "Id,Name")] Location lokacija)
        {
            if (ModelState.IsValid)
            {
                service.Edit(lokacija);
                return RedirectToAction("Index");
            }
            return View(lokacija);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location lokacija = service.GetById(id);
            if (lokacija == null)
            {
                return HttpNotFound();
            }
            return View(lokacija);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult DeleteConfirmed(int id)
        {
            service.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
