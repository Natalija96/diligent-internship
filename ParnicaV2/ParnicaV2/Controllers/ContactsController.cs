﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using ParnicaV2.Models;
using ParnicaV2.Services;
using ParnicaV2.ViewModels;

namespace ParnicaV2.Controllers
{
    public class ContactsController : Controller
    {
        ServiceContact service = new ServiceContact();

        // GET: Kontakts
        [Authorize(Roles = ("Admin"))]
        public ActionResult Index()
        {         
            
            return View(ContactViewModel.ToDTOlist(service.List()));
        }

        // GET: Kontakts/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact kontakt = service.FindById(id);
            if (kontakt == null)
            {
                return HttpNotFound();
            }
            return View(kontakt);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Create()
        {

            ViewBag.Kompanije = service.ListCompanies();      
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Create(ContactViewModel contact)
        {
       
            if (ModelState.IsValid)
            {
                service.Add(ContactViewModel.FromDTOtoModel(contact));             
                return RedirectToAction("Index");
            }
            ViewBag.Kompanije = service.ListCompanies();
            return View(contact);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = service.FindById(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            ViewBag.Kompanije = service.ListCompanies();
            return View(ContactViewModel.ToDTO(contact));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit(ContactViewModel contact)
        {
           
            if (ModelState.IsValid)
            {
                service.Edit(ContactViewModel.FromDTOtoModel(contact));
                return RedirectToAction("Index");
            }
            ViewBag.Kompanije = service.ListCompanies();
            return View(contact);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact kontakt = service.FindById(id);
            if (kontakt == null)
            {
                return HttpNotFound();
            }
            return View(kontakt);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult DeleteConfirmed(int id)
        {
            service.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
