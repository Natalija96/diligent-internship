﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ParnicaV2.Models;
using ParnicaV2.Services;

namespace ParnicaV2.Controllers
{
    public class CompaniesController : Controller
    {
        ServiceCompany service = new ServiceCompany();

        [Authorize(Roles=("Admin"))]
        public ActionResult Index()
        {
            return View(service.List());
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company kompanija = service.GetById(id);
            if (kompanija == null)
            {
                return HttpNotFound();
            }
            return View(kompanija);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Create([Bind(Include = "Id,Name,Address")] Company kompanija)
        {
            if (ModelState.IsValid)
            {
                service.Add(kompanija);
                return RedirectToAction("Index");
            }

            return View(kompanija);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company kompanija = service.GetById(id);
            if (kompanija == null)
            {
                return HttpNotFound();
            }
            return View(kompanija);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult Edit([Bind(Include = "Id,Name,Address")] Company kompanija)
        {
            if (ModelState.IsValid)
            {
                service.Edit(kompanija);
                return RedirectToAction("Index");
            }
            return View(kompanija);
        }

        [Authorize(Roles = ("Admin"))]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company kompanija = service.GetById(id);
            if (kompanija == null)
            {
                return HttpNotFound();
            }
            return View(kompanija);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Admin"))]
        public ActionResult DeleteConfirmed(int id)
        {
            service.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
