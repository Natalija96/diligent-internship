﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ParnicaV2.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ParnicaV2.Services;
using ParnicaV2.ViewModels;

using System.Globalization;

using System.Security.Claims;
using System.Data.Entity.Validation;

namespace ParnicaV2.Controllers
{
    public class ApplicationUsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        ServiceUser service = new ServiceUser();

        [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            return View(UsersViewModel.ListUsersToDTO(db.Users.ToList()));
        }

        [Authorize]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = service.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.Roles = service.GetRoles(applicationUser.Id);
            return View(UsersViewModel.ToDTO(applicationUser));
        }

        // GET: ApplicationUsers/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.Roles = service.GetRoles();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(RegisterViewModel model, string[] roles)
        {
            if (ModelState.IsValid && roles!=null)
            {

                var user = RegisterViewModel.FromDTO(model);                  
                if (service.Add(user, model.Password, roles))
                {                   
                    return RedirectToAction("Index", "ApplicationUsers");
                }
            }
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            ViewBag.Roles = roleManager.Roles.ToList();
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = service.FindById(id);
        
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            ViewBag.Roles = service.GetRoles();
            return View(UserViewModelEdit.ToDTO(applicationUser));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(UserViewModelEdit user, string[] roles)
        {
          
            if (ModelState.IsValid)
            {
                service.Edit(UserViewModelEdit.FromDTO(user), roles);
                return RedirectToAction("Index");
            }
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            ViewBag.Roles = roleManager.Roles.ToList();
            return View(user);
        }



        // GET: ApplicationUsers/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = service.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(UsersViewModel.ToDTO(applicationUser));
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(string id)
        {
            service.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
