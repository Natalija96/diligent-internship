﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class LitigationViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Location:")]
        public int LocationId { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Judge:")]
        public int JudgeId { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Defendant:")]
        public int? DefendantId { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Prosecutor:")]
        public int? ProsecutorId { get; set; }
   
        [Required(ErrorMessage = "Value cannot be null")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Date and time:")]
        public DateTime DateAndTime { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Procedure type:")]
        public int ProcedureTypeId { get; set; }

        [Display(Name = "Note:")]
        public string Note { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Court type:")]
        public string CourtType { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name = "Procedure ID:")]
        public string ProcedureId { get; set; }

        [Required(ErrorMessage = "Value cannot be null")]
        [Display(Name ="Courtroom number:")]
        public int CourtroomNumber { get; set; }

        public static Litigation FromDTOtoModel(LitigationViewModel p)
        {

            return new Litigation
            {
                Id = p.Id,
                LocationId = p.LocationId,
                JudgeId = p.JudgeId,
                DefendantId = p.DefendantId,
                ProsecutorId = p.ProsecutorId,
                ProcedureId = p.ProcedureId,
                ProcedureTypeId = p.ProcedureTypeId,
                CourtroomNumber = p.CourtroomNumber,
                CourtType = p.CourtType,
                DateAndTime=p.DateAndTime,
                Note=p.Note

            };
        }

        public static LitigationViewModel ToDTO(Litigation p)
        {

            return new LitigationViewModel
            {
                Id = p.Id,
                LocationId = p.LocationId,
                JudgeId = p.JudgeId,
                DefendantId = p.DefendantId,
                ProsecutorId = p.ProsecutorId,
                ProcedureId = p.ProcedureId,
                ProcedureTypeId = p.ProcedureTypeId,
                CourtroomNumber = p.CourtroomNumber,
                CourtType = p.CourtType,
                DateAndTime = p.DateAndTime,
                Note = p.Note

            };

        }
        public static IEnumerable<LitigationViewModel> ToDTOlist(IEnumerable<Litigation> litigations)
        {

            List<LitigationViewModel> l = new List<LitigationViewModel>();
            foreach(var en in litigations)
            {
                l.Add(LitigationViewModel.ToDTO(en));
            }
            return l;

        }
    }
}