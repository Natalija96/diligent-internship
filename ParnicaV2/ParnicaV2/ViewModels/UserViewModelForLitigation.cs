﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class UserViewModelForLitigation
    {
       public string Id { get; set; }
       public string FullName { get; set; }

        public static UserViewModelForLitigation toDTO(ApplicationUser user)
        {
            return new UserViewModelForLitigation
            {
                Id = user.Id,
                FullName = user.Name + "  " + user.LastName
            };
        
        }
        public static IEnumerable<UserViewModelForLitigation> toDTOList(IEnumerable<ApplicationUser> users)
        {
            List<UserViewModelForLitigation> l = new List<UserViewModelForLitigation>();
            foreach(var e in users)
            {
                l.Add(UserViewModelForLitigation.toDTO(e));
            }
            return l;
        }
    }
}