﻿namespace ParnicaV2.ViewModels
{
    public class ContactViewModelForLitigation 
    {
        public int? Id { get; set; }
        public string FullName { get; set; }
    }
}