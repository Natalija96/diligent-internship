﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class LitigationViewModelDetails
    {

        public int Id { get; set; }

        [Display(Name = "Location:")]
        public  string Location { get; set; }

        [Display(Name = "Judge:")]
        public ContactViewModelForLitigation Judge { get; set; }

        [Display(Name = "Defendant:")]
        public ContactViewModelForLitigation Defendant { get; set; }

        [Display(Name = "Prosecutor:")]
        public ContactViewModelForLitigation Prosecutor { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Date and time:")]
        public DateTime DateAndTime { get; set; }
        public  ICollection<UserViewModelForLitigation> Lawyers { get; set; }

        [Display(Name = "Procedure type:")]
        public string ProcedureType { get; set; }

        [Display(Name = "Note:")]
        public string Note { get; set; }

        [Display(Name = "Court type:")]
        public string CourtType { get; set; }

        [Display(Name = "Procedure ID:")]
        public string ProcedureId { get; set; }

        [Display(Name = "Courtroom number:")]
        public int CourtroomNumber { get; set; }

        public LitigationViewModelDetails()
        {
            this.Lawyers = new HashSet<UserViewModelForLitigation>();
        }

        public static LitigationViewModelDetails ToDTO(Litigation p)
        {
            return new LitigationViewModelDetails
            {
                Id = p.Id,
                Location = p.Location.Name,
                Prosecutor = new ContactViewModelForLitigation { Id=p.ProsecutorId,FullName= p.Prosecutor.Name },
                Defendant = new ContactViewModelForLitigation { Id=p.DefendantId, FullName=p.Defendant.Name },
                Judge = new ContactViewModelForLitigation { Id=p.JudgeId, FullName= p.Judge.Name },
                ProcedureType = p.ProcedureType.Name,
                DateAndTime = p.DateAndTime,
                ProcedureId = p.ProcedureId,
                Note = p.Note,
                CourtroomNumber = p.CourtroomNumber,
                CourtType = p.CourtType,
                Lawyers = p.Lawyers.Select(x => new UserViewModelForLitigation { Id=x.Id,FullName=x.Name + " " + x.LastName}).ToList()
            };
        }
    }
}
