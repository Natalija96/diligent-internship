﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
 
    public class UserViewModelEdit
    {
        [Display(Name = "Id")]
        [Required]
        public string Id { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "LastName")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }


        public static UserViewModelEdit ToDTO(ApplicationUser e)
        {     
            return new UserViewModelEdit
            {
                Id = e.Id,
                Name = e.Name,
                LastName = e.LastName,
                PhoneNumber = e.PhoneNumber,
            };
        }

        internal static ApplicationUser FromDTO(UserViewModelEdit e)
        {       
            return new ApplicationUser
            {
                Id = e.Id,
                Name = e.Name,
                LastName = e.LastName,
                PhoneNumber = e.PhoneNumber,
            };
        }
    }
    
}