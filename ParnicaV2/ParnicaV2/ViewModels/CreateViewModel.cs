﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class CreateViewModel
    {
        public List<Contact> Kontaki { get; set; }
        public List<Location> Lokacija { get; set; }

        public List<ProcedureType> TipoviPostupka { get; set; }

        //  public List<Kontakt> Tuzioci{ get; set; }
    }
}