﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class ContactViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
   
        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }
   
        public string Address { get; set; }

        public string Email { get; set; }

        public bool LegalEntity { get; set; }

        public string Occupation { get; set; }

        public int? CompanyId { get; set; }

        public static Contact FromDTOtoModel(ContactViewModel c )
        {
            return new Contact
            {
                Id = c.Id,
                Name = c.Name,
                PhoneNumber1 = c.PhoneNumber1,
                PhoneNumber2 = c.PhoneNumber2,
                Address = c.Address,
                Occupation = c.Occupation,
                LegalEntity = c.LegalEntity,
                CompanyId = c.CompanyId
            };
        }

        public static IEnumerable<ContactViewModel> ToDTOlist(IEnumerable<Contact> enumerable)
        {
            List<ContactViewModel> l = new List<ContactViewModel>();
            foreach (var en in enumerable)
            {
                l.Add(ContactViewModel.ToDTO(en));
            }
            return l;
        }

        public static ContactViewModel ToDTO(Contact c)
        {
            return new ContactViewModel
            {
                Id = c.Id,
                Name = c.Name,
                PhoneNumber1 = c.PhoneNumber1,
                PhoneNumber2 = c.PhoneNumber2,
                Address = c.Address,
                Occupation = c.Occupation,
                LegalEntity = c.LegalEntity,
                CompanyId = c.CompanyId
            };
        }

    }
}