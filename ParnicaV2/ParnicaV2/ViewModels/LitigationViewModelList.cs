﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class LitigationViewModelList
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Date and time:")]
        public DateTime DateAndTime { get; set; }

        [Display(Name = "Note:")]
        public string Note { get; set; }

        [Required]
        [Display(Name = "Court type:")]
        public string CourtType { get; set; }

        [Required]
        [Display(Name = "Procedure ID:")]
        public string ProcedureId { get; set; }

        [Required]
        [Display(Name = "Courtroom number:")]
        public int CourtroomNumber { get; set; }
        public static Litigation FromDTOtoModel(LitigationViewModel p)
        {

            return new Litigation
            {
                Id = p.Id,       
                ProcedureId = p.ProcedureId,
                CourtroomNumber = p.CourtroomNumber,
                CourtType = p.CourtType,
                DateAndTime = p.DateAndTime,
                Note = p.Note

            };
        }

        public static LitigationViewModelList ToDTO(Litigation p)
        {

            return new LitigationViewModelList
            {
                Id = p.Id,
                ProcedureId = p.ProcedureId,
                CourtroomNumber = p.CourtroomNumber,
                CourtType = p.CourtType,
                DateAndTime = p.DateAndTime,
                Note = p.Note

            };

        }
        public static IEnumerable<LitigationViewModelList> ToDTOlist(IEnumerable<Litigation> litigations)
        {

            List<LitigationViewModelList> l = new List<LitigationViewModelList>();
            foreach (var en in litigations)
            {
                l.Add(LitigationViewModelList.ToDTO(en));
            }
            return l;

        }
    }
}