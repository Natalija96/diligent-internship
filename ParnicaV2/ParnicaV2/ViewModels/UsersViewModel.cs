﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using ParnicaV2.Models;

namespace ParnicaV2.ViewModels
{
    public class UsersViewModel
    {
        [EmailAddress]
        [Display(Name = "Id")]
        public string Id { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public static List<UsersViewModel> ListUsersToDTO(List<ApplicationUser> list)
        {
            List < UsersViewModel > l = new List<UsersViewModel>();

            foreach(var e in list)
            {
                l.Add(UsersViewModel.ToDTO(e));
            }
            return l;
        }

        public static UsersViewModel ToDTO(ApplicationUser e)
        {          
            return new UsersViewModel
            {
                Id = e.Id,
                Name = e.Name,
                LastName = e.LastName,
                PhoneNumber = e.PhoneNumber,
                Email = e.Email,
            };
        }
    }
}