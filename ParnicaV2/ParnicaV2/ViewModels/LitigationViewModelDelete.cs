﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.ViewModels
{
    public class LitigationViewModelDelete
    {
        public int Id { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Date and time:")]
        public DateTime DateAndTime { get; set; }

        [Display(Name = "Procedure type:")]
        public string ProcedureType { get; set; }

        [Display(Name = "Note:")]
        public string Note { get; set; }

        [Display(Name = "Court type:")]
        public string CourtType { get; set; }

        [Display(Name = "Procedure ID:")]
        public string ProcedureId { get; set; }

        [Display(Name = "Courtroom number:")]
        public int CourtroomNumber { get; set; }
        public static LitigationViewModelDelete ToDTO(Litigation p)
        {
            return new LitigationViewModelDelete
            {
                Id = p.Id,       
                DateAndTime = p.DateAndTime,
                ProcedureId = p.ProcedureId,
                Note = p.Note,
                CourtroomNumber = p.CourtroomNumber,
                CourtType = p.CourtType
            };
        }
    }
}