﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.Repositories
{
    public class RepositoryCompany : IRepositoryKompanija, IDisposable
    {
        private ApplicationDbContext _dbContext;

        public RepositoryCompany(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Company GetById(int? id)
        {
            return _dbContext.Companies.Find(id);
        }

        public IEnumerable<Company> List()
        {
            return _dbContext.Companies.ToList();
        }

        public IEnumerable<Company> List(System.Linq.Expressions.Expression<Func<Company, bool>> predicate)
        {
            return _dbContext.Companies
                   .Where(predicate)
                   .AsEnumerable();
        }

        public void Add(Company entity)
        {
            _dbContext.Companies.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Edit(Company entity)
        {
            _dbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void Delete(Company entity)
        {
            _dbContext.Companies.Remove(entity);
            _dbContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}