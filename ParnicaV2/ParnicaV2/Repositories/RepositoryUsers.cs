﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ParnicaV2.Models;

namespace ParnicaV2.Repositories
{
    public class RepositoryUsers : IRepositoryUser, IDisposable
    {
        private ApplicationDbContext _dbContext;

        public RepositoryUsers(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Delete(ApplicationUser entity)
        {
            _dbContext.Users.Remove(entity);
            _dbContext.SaveChanges();
        }

        public ApplicationUser GetById(string id)
        {
            return _dbContext.Users.Find(id);
        }

        public IEnumerable<ApplicationUser> List()
        {
            return _dbContext.Users.ToList();
        }

 
        public void Edit(ApplicationUser user)
        {
            ApplicationUser u = _dbContext.Users.Find(user.Id);
            u.PhoneNumber = user.PhoneNumber;
            u.Name = user.Name;
            u.LastName = user.LastName;
            _dbContext.Entry(u).State = System.Data.Entity.EntityState.Modified;
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}