﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ParnicaV2.Models;

namespace ParnicaV2.Repositories
{
    public class RepositoryLocation : IRepositoryLokacija, IDisposable
    {
        private ApplicationDbContext dbContext;

        public RepositoryLocation(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Add(Location entity)
        {
            this.dbContext.Locations.Add(entity);
            dbContext.SaveChanges();
        }

        public void Delete(Location entity)
        {
            this.dbContext.Locations.Remove(entity);
            dbContext.SaveChanges();
        }

      
        public Location GetById(int? id)
        {
            return this.dbContext.Locations.Find(id);
        }

        public IEnumerable<Location> List()
        {
            return this.dbContext.Locations.ToList();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Edit(Location entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }
}