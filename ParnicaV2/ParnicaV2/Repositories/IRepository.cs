﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ParnicaV2.Repositories
{
    public interface IRepositoryParnica : IDisposable
    {
        Litigation GetById(int? id);
        void Add(Litigation entity);
        void Edit(Litigation entity);
        void Delete(Litigation entity);       
        IEnumerable<Litigation> List();
        IEnumerable<Litigation> List(Expression<Func<Litigation, bool>> predicate);
      
    }
    public interface IRepositoryKontakt:IDisposable
    {
        Contact GetById(int? id);
        void Add(Contact entity);
        void Delete(Contact entity);
        void Edit(Contact entity);
        IEnumerable<Contact> List();
        IEnumerable<Contact> List(Expression<Func<Contact, bool>> predicate);
       
    }
    public interface IRepositoryKompanija: IDisposable
    {
        Company GetById(int? id);  
        void Add(Company entity);
        void Delete(Company entity);
        void Edit(Company entity);

        IEnumerable<Company> List();

        IEnumerable<Company> List(Expression<Func<Company, bool>> predicate);
    }
    public interface IRepositoryLokacija: IDisposable
    {
        Location GetById(int? id);   
        void Add(Location entity);
        void Delete(Location entity);
        void Edit(Location entity);
        IEnumerable<Location> List();
    }
    public interface IRepositoryTipPostupka: IDisposable
    {
        ProcedureType GetById(int? id);
   
        void Add(ProcedureType entity);
        void Delete(ProcedureType entity);
        void Edit(ProcedureType entity);

        IEnumerable<ProcedureType> List();
    }
    public interface IRepositoryUser: IDisposable
    {
        ApplicationUser GetById(string id);
        IEnumerable<ApplicationUser> List();
       // IEnumerable<ApplicationUser> List(Expression<Func<Contact, bool>> predicate);
        // void Add(T entity);
        void Delete(ApplicationUser entity);
        void Edit(ApplicationUser entity);
    }
}