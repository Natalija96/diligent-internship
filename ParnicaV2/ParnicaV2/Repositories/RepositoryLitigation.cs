﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ParnicaV2.Repositories
{
  
    public class RepositoryLitigation : IRepositoryParnica, IDisposable
    { 

        private ApplicationDbContext _dbContext;

        public RepositoryLitigation(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(Litigation entity)
        {
            _dbContext.Litigations.Add(entity);
            _dbContext.SaveChanges();
        }
        public void Edit(Litigation entity)
        {
            _dbContext.Litigations.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }
        public void RemoveRelationship(Litigation entity, ApplicationUser user )
        {
            _dbContext.Litigations.Attach(entity);
            _dbContext.Entry(entity).Collection(c => c.Lawyers).Load();
            entity.Lawyers.Remove(user);
            _dbContext.SaveChanges();
        }
        public void AddRelationship(Litigation entity, ApplicationUser user)
        {
            _dbContext.Litigations.Attach(entity);
            _dbContext.Entry(entity).Collection(c => c.Lawyers).Load();
            entity.Lawyers.Add(user);
             _dbContext.SaveChanges();
        }

        public void Delete(Litigation entity)
        {
            _dbContext.Litigations.Remove(entity);
            _dbContext.SaveChanges();
        }
   
        public Litigation GetById(int? id)
        {
            return _dbContext.Litigations.Find(id);
        }

        public IEnumerable<Litigation> List()
        {
            return _dbContext.Litigations.ToList();
        }

        public IEnumerable<Litigation> List(Expression<Func<Litigation, bool>> predicate)
        {
            return _dbContext.Litigations.Where(predicate);
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}