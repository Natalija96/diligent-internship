﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ParnicaV2.Models;

namespace ParnicaV2.Repositories
{
    public class RepositoryProcedureType : IRepositoryTipPostupka, IDisposable
    {
        private ApplicationDbContext dbContext;

        public RepositoryProcedureType(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Add(ProcedureType entity)
        {
            this.dbContext.ProcedureTypes.Add(entity);
            dbContext.SaveChanges();
        }

        public void Delete(ProcedureType entity)
        {
            this.dbContext.ProcedureTypes.Remove(entity);
            dbContext.SaveChanges();
        }

     

        public ProcedureType GetById(int? id)
        {
            return this.dbContext.ProcedureTypes.Find(id);
        }

        public IEnumerable<ProcedureType> List()
        {
            return this.dbContext.ProcedureTypes.ToList();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Edit(ProcedureType tipPostupka)
        {
            dbContext.Entry(tipPostupka).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}