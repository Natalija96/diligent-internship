﻿using ParnicaV2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ParnicaV2.Repositories
{
    public class RepositoryContact : IRepositoryKontakt, IDisposable
    {
        private ApplicationDbContext _dbContext;

        public RepositoryContact(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Contact GetById(int? id)
        {
            return _dbContext.Contacts.Find(id);
        }

        public IEnumerable<Contact> List()
        {
            return _dbContext.Contacts;
        }

        public IEnumerable<Contact> List(System.Linq.Expressions.Expression<Func<Contact, bool>> predicate)
        {
            return _dbContext.Contacts
                   .Where(predicate)
                   .AsEnumerable();
        }

        public void Add(Contact entity)
        {
            _dbContext.Contacts.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Edit(Contact entity)
        {

            _dbContext.Contacts.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void Delete(Contact entity)
        {
            _dbContext.Contacts.Remove(entity);
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}