﻿using ParnicaV2.Models;
using ParnicaV2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.Services
{
    public class ServiceLocation
    {
        ApplicationDbContext _dbContext = new ApplicationDbContext();
        RepositoryLocation _repositoryLocation;
        public RepositoryLitigation _repositoryLitigation;
        public ServiceLocation()
        {
            _repositoryLocation = new RepositoryLocation(_dbContext);
            _repositoryLitigation = new RepositoryLitigation(_dbContext);
        }
        public void Add(Location entity)
        {
            this._repositoryLocation.Add(entity);
           
        }

        public void Delete(int id)
        {
            IEnumerable<Litigation> l = _repositoryLitigation.List();
            IEnumerable<Litigation> filter = l.Where(item => item.LocationId == id).ToList();
            foreach (var el in filter)
            {
                _repositoryLitigation.Delete(el);
            }
            Location loc = _repositoryLocation.GetById(id);
            this._repositoryLocation.Delete(loc);
        }

        public Location GetById(int? id)
        {
            return this._repositoryLocation.GetById(id);
        }

        public IEnumerable<Location> List()
        {
            return this._repositoryLocation.List();
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        internal void Edit(Location location)
        {
            _repositoryLocation.Edit(location);
        }
    }
}