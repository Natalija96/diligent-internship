﻿using ParnicaV2.Models;
using ParnicaV2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ParnicaV2.Services
{
    public class ServiceLitigation
    {
        public ApplicationDbContext _dbContext;
        public RepositoryContact _repositoryContact;
        public RepositoryLitigation _repositorLitigation;
        public RepositoryLocation _repositoryLocation;
        RepositoryProcedureType _repositoryProcedureType;
        RepositoryUsers _repositoryUser;
        public ServiceLitigation()
        {
            _dbContext = new ApplicationDbContext();
            _repositoryContact = new RepositoryContact(_dbContext);
            _repositorLitigation = new RepositoryLitigation(_dbContext);
            _repositoryLocation = new RepositoryLocation(_dbContext);
            _repositoryProcedureType = new RepositoryProcedureType(_dbContext);
            _repositoryUser=new RepositoryUsers(_dbContext);
        }
        public void Add(Litigation litigation, string[] strUsersIds)
        {        
            foreach (var id in strUsersIds)
            {
                ApplicationUser user = _repositoryUser.GetById(id);
                litigation.Lawyers.Add(user);
            }
            _repositorLitigation.Add(litigation);
        }
        public void Edit(Litigation litigation, string[] advokati, string[] neadvokati)
        {
            if (advokati != null)
            {
                foreach (var id in advokati)
                {
                    ApplicationUser user = _repositoryUser.GetById(id);
                    _repositorLitigation.RemoveRelationship(litigation, user);
                }
            }
            if (neadvokati != null)
            {
                foreach (var id in neadvokati)
                {
                    ApplicationUser user = _repositoryUser.GetById(id);
                    _repositorLitigation.AddRelationship(litigation, user);
                }
            }
            _repositorLitigation.Edit(litigation);
          
        }

        public Litigation GetById(int? id)
        {
            return _repositorLitigation.GetById(id);
        }
        public void Delete(int id)
        {
            Litigation p = _repositorLitigation.GetById(id);
           _repositorLitigation.Delete(p);
        }

        public IEnumerable<Litigation> List()
        {
            return _repositorLitigation.List();
        }
        public IEnumerable<Contact> ListContacts()
        {
            return _repositoryContact.List();
        }
        public IEnumerable<Location> ListLocations()
        {
            return _repositoryLocation.List();
        }
        public IEnumerable<ProcedureType> ListProcedureTypes()
        {
            return _repositoryProcedureType.List();
        }
        public IEnumerable<ApplicationUser> ListUsers()
        {
            return _repositoryUser.List();
        }
        public IEnumerable<Litigation> List(Expression<Func<Litigation, bool>> predicate)
        {
            return _repositorLitigation.List(predicate);
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}