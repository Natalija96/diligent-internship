﻿using ParnicaV2.Models;
using ParnicaV2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.Services
{
    public class ServiceProcedureType
    {
          
        ApplicationDbContext _dbContext = new ApplicationDbContext();
        RepositoryProcedureType _repositoryTProcedureType;
        public RepositoryLitigation _repositoryLitigation;
        public ServiceProcedureType()
        {
            _repositoryTProcedureType = new RepositoryProcedureType(_dbContext);
            _repositoryLitigation = new RepositoryLitigation(_dbContext);
        }
        public void Add(ProcedureType entity)
        {
            this._repositoryTProcedureType.Add(entity);

        }

        public void Delete(int id)
        {
            IEnumerable<Litigation> l = _repositoryLitigation.List();
            IEnumerable<Litigation> filter = l.Where(item=>item.ProcedureTypeId==id).ToList();
            foreach (var el in filter)
            {
                _repositoryLitigation.Delete(el);
            }
            ProcedureType type = _repositoryTProcedureType.GetById(id);
            this._repositoryTProcedureType.Delete(type);
        }

        public ProcedureType GetById(int? id)
        {
            return this._repositoryTProcedureType.GetById(id);
        }

        public IEnumerable<ProcedureType> List()
        {
            return this._repositoryTProcedureType.List();
        }

        internal void Dispose()
        {
            _dbContext.Dispose();
        }

        internal void Edit(ProcedureType procedureType)
        {
            _repositoryTProcedureType.Edit(procedureType);
        }
    }
}