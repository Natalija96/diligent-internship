﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ParnicaV2.Models;
using ParnicaV2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;


namespace ParnicaV2.Services
{
    public class ServiceUser
    {
        ApplicationDbContext _dbContext = new ApplicationDbContext();
        RepositoryUsers _repositoryUser;
        ApplicationUserManager userManager;
        public ServiceUser()
        {
            _repositoryUser = new RepositoryUsers(_dbContext);
             userManager =new ApplicationUserManager(new UserStore<ApplicationUser>(_dbContext)); 
        }
        public IEnumerable<ApplicationUser> List()
        {
            return this._repositoryUser.List();
        }

        internal ApplicationUser FindById(string id)
        {
            return _repositoryUser.GetById(id);
        }

        internal void Delete(string id)
        {
            ApplicationUser u = _repositoryUser.GetById(id);
            _repositoryUser.Delete(u);

        }

        internal void Edit(ApplicationUser user, string[] roles)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_dbContext));
         
            if (roles != null)
            {
                string[] roleRemove = GetRoles(user.Id);
                userManager.RemoveFromRoles(user.Id, roleRemove);
                userManager.AddToRoles(user.Id, roles);
            }
            _repositoryUser.Edit(user);

        }
        public bool Add(ApplicationUser user, string password, string[] roles)
        {

            var result = userManager.Create(user, password);
            if (result.Succeeded)
            {
                foreach (var role in roles)
                    userManager.AddToRole(user.Id, role);
                return true;
            }
            return false;
        }
        public string[] GetRoles(string userId)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_dbContext));

            string[] roles = _repositoryUser.GetById(userId)
                .Roles.Select(x => (roleManager.FindById(x.RoleId)).Name).ToArray<string>();

            return roles;
        }
        public IEnumerable<IdentityRole> GetRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_dbContext));
            return  roleManager.Roles.ToList();
        }

        internal void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}