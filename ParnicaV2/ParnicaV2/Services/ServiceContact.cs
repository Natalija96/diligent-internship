﻿using ParnicaV2.Models;
using ParnicaV2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.Services
{
    public class ServiceContact
    {
        public ApplicationDbContext _dbContext;
        public RepositoryContact _repositoryContact;
        public RepositoryCompany _repositoryCompany;
        public RepositoryLitigation _repositoryLitigation;
        public ServiceContact()
        {
            _dbContext = new ApplicationDbContext();
            _repositoryContact = new RepositoryContact(_dbContext);
            _repositoryCompany = new RepositoryCompany(_dbContext);
            _repositoryLitigation = new RepositoryLitigation(_dbContext);
        }
        public IEnumerable<Company> ListCompanies()
        {
            return _repositoryCompany.List();
        }
        public IEnumerable<Contact> List()
        {
            return _repositoryContact.List();
        }
        public void  Add(Contact contact)
        {
            _repositoryContact.Add(contact);
        }       
        public Contact FindById(int? id)
        {
            return _repositoryContact.GetById(id); 
        }
        public void Edit(Contact contact)
        {
            _repositoryContact.Edit(contact);
        }
        public  void Delete(int? id)
        {
            IEnumerable<Litigation> l=_repositoryLitigation.List();
            IEnumerable<Litigation> filter=l.Where(item => item.DefendantId == id || item.ProsecutorId == id || item.JudgeId == id).ToList();
           foreach(var el in filter)
            {
                _repositoryLitigation.Delete(el);
            }
            Contact k = _repositoryContact.GetById(id);
            _repositoryContact.Delete(k);
        }
        public void Dispose()
        {
            _dbContext.Dispose(); 
        }
    }
}