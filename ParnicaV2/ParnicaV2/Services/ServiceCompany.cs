﻿using ParnicaV2.Models;
using ParnicaV2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParnicaV2.Services
{
    public class ServiceCompany
    {
        ApplicationDbContext _dbContext = new ApplicationDbContext();
        RepositoryCompany _repositoryCompany;
        public RepositoryContact _repositoryContact;


        public ServiceCompany()
        {
            _repositoryCompany = new RepositoryCompany(_dbContext);
            _repositoryContact = new RepositoryContact(_dbContext);
        }
        public void Add(Company entity)
        {
            this._repositoryCompany.Add(entity);

        }
        public void Delete(int id)
        {
            IEnumerable<Contact> l = _repositoryContact.List();
            IEnumerable<Contact> filter = l.Where(item => item.CompanyId == id).ToList();
            foreach (var el in filter)
            {
                el.CompanyId = null;
                _repositoryContact.Edit(el);
            }
            Company k = _repositoryCompany.GetById(id);
            this._repositoryCompany.Delete(k);
        }

        public Company GetById(int? id)
        {
            return this._repositoryCompany.GetById(id);
        }

        public IEnumerable<Company> List()
        {
            return this._repositoryCompany.List();
        }

        internal void Dispose()
        {
            _repositoryCompany.Dispose();
        }

        internal void Edit(Company company)
        {
            _repositoryCompany.Edit(company);
        }
    }
}