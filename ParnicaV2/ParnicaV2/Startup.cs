﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ParnicaV2.Startup))]
namespace ParnicaV2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
