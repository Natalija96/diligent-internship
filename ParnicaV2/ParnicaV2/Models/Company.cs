﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.Models
{
    public class Company
    {
        public  int Id { get; set; }
        [Required]
        public  string Name { get; set; }
        [Required]
        public  string Address { get; set; }
        public virtual ICollection<Contact> Employees { get; set; }
        public Company()
        {
            this.Employees = new HashSet<Contact>();
        }
    }
}