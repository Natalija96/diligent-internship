﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.Models
{
    public class Contact
    {
        public  int Id { get; set; }
        [Required]
        public  string Name { get; set; }
        public  string PhoneNumber1 { get; set; }

        public  string PhoneNumber2 { get; set; }

        public  string Address { get; set; }

        public  string Email { get; set; }

        public  bool LegalEntity { get; set; }

        public  string Occupation { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public Contact()
        {

        }


    }
}