﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.Models
{
    public class Location
    {
        public  int Id { get; set; }
        [Required]
        public  string Name { get; set; }
        public virtual ICollection<Litigation> Litigations { get; set; }
        public Location()
        {
            this.Litigations = new HashSet<Litigation>();
        }
    }
}