﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParnicaV2.Models
{
    public class Litigation
    {
        public  int Id { get; set; }

        [Required]
        public int  LocationId { get; set; }
        public virtual Location Location { get; set; }

        [Required]
        public int JudgeId { get; set; }
        public virtual Contact Judge { get; set; }

        public int? DefendantId { get; set; }
        public virtual Contact Defendant { get; set; }

        public int? ProsecutorId { get; set; }
        public virtual Contact Prosecutor { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public  DateTime DateAndTime { get; set; }
        public virtual ICollection<ApplicationUser> Lawyers { get; set; }

        [Required]
        public int ProcedureTypeId { get; set; }
        public virtual ProcedureType ProcedureType { get; set; }

        public  string Note { get; set; }

        [Required]
        public  string CourtType { get; set; }

        [Required]
        public  string ProcedureId { get; set; }

        [Required]
        public  int CourtroomNumber { get; set; }

        public Litigation()
        {
            this.Lawyers = new HashSet<ApplicationUser>();
        }
    }
}